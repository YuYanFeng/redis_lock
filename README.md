# Redis锁的各种实现：分布式锁，自旋锁，可重入锁，异步续命


#### 介绍
使用Redis实现分布式锁，自旋锁，可重入锁，异步续命


#### 设计架构
RedisLock接口有两个默认需要实现的方法：tryLock（加锁）和releaseLock（释放锁）

分布式锁实现：RedisLockImplSingleThread [不可重入分布式锁](https://gitee.com/yayiboy/redis_lock/blob/master/java/redis/impl/RedisLockImplSingleThread.java)

可重入锁实现：RedisLockImplReentry [可重入分布式锁实现](https://gitee.com/yayiboy/redis_lock/blob/master/java/redis/impl/RedisLockImplReentry.java)

自旋锁实现：RedisLockImplSpin [可重入自旋锁实现](https://gitee.com/yayiboy/redis_lock/blob/master/java/redis/impl/RedisLockImplSpin.java)

异步续命实现：RedisLockImplAsynTime [可重入自旋锁的异步续命](https://gitee.com/yayiboy/redis_lock/blob/master/java/redis/impl/RedisLockImplAsynTime.java)


#### 说明介绍

所有的实现方式均为简略版，提供的只是一个大致的思路。实际使用以及生产环境下，需要根据情况以及业务逻辑进行设计。  
1、简单分布式实现：利用redis的setNX，如果值不存在则设置值并返回1，表示加锁成功。如果值存在则返回0，表示加锁失败；  
2、可重入分布式锁：为了使当前线程可以多次获得锁，实现锁的可重入效果，在简单分布式锁的基础上，使用ThreadLocal变量记录当前线程的重入锁次数，每次加锁次数加一，释放锁次数减一。只有当次数减为0，这个锁才会被完全释放；  
3、可重入自旋锁实现：部分场景下线程需要争取到锁才能继续进行，这个时候就需要自旋锁。在可重入锁的基础上，增加自选操作while循环不断尝试，只有拿到了锁才退出循环。这种方式对于系统来说并不友好，在没有拿到锁的情况下会持续占用资源。但为了保证逻辑的正确和业务的正常，自旋锁还是有使用的必要。  
4、锁的异步续命：在获取到锁之后，由于数据过大、IO太慢、业务过于复杂等情况，锁到了过期时间。对于redis锁，我们必须设置过期时间，避免因为网络波动或者程序异常造成的死锁，导致整个系统崩溃，业务数据崩塌。这种情况下，为了避免锁提前过期，可以使用异步续命方式。新增一个线程用于持续性刷新当前锁的过期时间，直到锁被释放线程才退出。  

