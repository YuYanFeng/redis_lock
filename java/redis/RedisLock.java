package com.redis.distribute.lock.demo.redis;

import java.util.concurrent.TimeUnit;

/**
 * @author : owoYam
 * @date : 2020/12/22 15:03
 */
public interface RedisLock {

    /**
     * redis尝试加锁
     * @param key 获取的key
     * @param timeout 过期时间
     * @param unit 时间的单位
     * @return 尝试加锁是否成功
     */
    boolean tryLock(String key, long timeout, TimeUnit unit);

    /**
     * redis释放锁
     * @param key 获取的key
     */
    void releaseLock(String key);
}
